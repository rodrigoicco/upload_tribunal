


<?php   include("conexao.php"); ?>

<!doctype html>
<html lang="pt-br">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Documentos</title>
  </head>
  <body>
<div class="container">
<h1>Upload de Arquivos</h1>
<form action="upload.php" method="POST" enctype="multipart/form-data">
<div class="mb-3">
  <label for="formFileMultiple" class="form-label">Multiple files input example</label>
  Arquivo: <input class="form-control" type="file" name="arquivo" id="formFileMultiple" multiple>
</div>
  <input type="submit" value="Salvar">
</form>

<?php if(isset($msg) && $msg != false) echo "<p> $msg </p>"; ?>
</div>
</body>
</html>

